import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import org.apache.log4j.Logger;
import java.util.*;

@RestController
@EnableAutoConfiguration
public class Hello {

	private static final Logger LOGGER = Logger.getLogger(Hello.class);

	@RequestMapping("/")
	String home() {
		String newLine = System.getProperty("line.separator");//This will retrieve line separator dependent on OS.
		//System.out.println("#$#$#STARTML#$#$#" + newLine + "same message line 1" + newLine + "same message line 2" + newLine + "same message line 3" + newLine + "#$#$#ENDML#$#$#");
		//System.out.println("different message 1");
		//System.out.println("different message 2");

		//System.out.println("#$#$#STARTML#$#$#");
		LOGGER.info("Logger same message line 1" + newLine + "Logger same message line 2" + newLine + "Logger same message line 3");
		//System.out.println("#$#$#ENDML#$#$#");

		LOGGER.info("Logger different message 1");
		LOGGER.info("Logger different message 2");
		return "Hello World log concat is working!!!";
	}

	@RequestMapping("/excep")
	String excep() {
		throw new EmptyStackException();
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Hello.class, args);
	}

}
